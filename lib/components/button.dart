import 'package:flutter/material.dart';

class ButtonReutilisable extends StatelessWidget {
  final VoidCallback onPressed;
  final String buttonText;
  final IconData icon;
  final Color backgroundColor;
  final Color textColor;
  final Color iconColor;

  const ButtonReutilisable({
    required this.onPressed,
    required this.buttonText,
    required this.icon,
    required this.backgroundColor,
    required this.textColor,
    required this.iconColor,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
        backgroundColor: backgroundColor,
        padding: const EdgeInsets.all(16.0),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Icon(icon, color: iconColor),
          const SizedBox(width: 8.0),
          Text(
            buttonText,
            style: TextStyle(
              color: textColor,
              fontSize: 15,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}
