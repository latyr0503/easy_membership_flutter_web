import 'package:flutter/material.dart';

class CardSection extends StatelessWidget {
  final String title;
  final String description;
  final String? imagePath;
  final Color titleColor;
  final Color descriptionColor;

  const CardSection({
    Key? key,
    required this.title,
    required this.description,
    this.imagePath,
    this.titleColor = const Color(0xFF021738),
    this.descriptionColor = const Color(0xFF555555),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300,
      color: Colors.white,
      padding: const EdgeInsets.all(25.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          if (imagePath != null)
            Image.asset(
              imagePath!,
              fit: BoxFit.cover,
            ),
          if (imagePath != null)
            const SizedBox(
              height: 10,
            ),
          Text(
            title,
            style: TextStyle(
              color: titleColor,
              fontSize: 25,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Text(
            description,
            style: TextStyle(
              color: descriptionColor,
              fontSize: 18,
              fontWeight: FontWeight.normal,
            ),
          ),
        ],
      ),
    );
  }
}
