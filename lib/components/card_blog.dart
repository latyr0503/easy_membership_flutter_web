import 'package:flutter/material.dart';

class CardBlog extends StatelessWidget {
  final String title;
  final String commentaires;
  final String admin;
  final String date;
  final String description;
  final String? imagePath;

  const CardBlog({
    Key? key,
    required this.title,
    required this.description,
    this.imagePath,
    required this.commentaires,
    required this.admin,
    required this.date,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        print('object');
      },
      child: SizedBox(
        width: 350,
        child: Column(
          children: [
            Stack(
              children: [
                Image.asset(
                  imagePath!,
                  fit: BoxFit.cover,
                ),
                Positioned(
                  bottom: 10,
                  left: 10,
                  child: Container(
                    color: const Color(0xFFFF3147),
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        const Icon(
                          Icons.calendar_month_sharp,
                          color: Colors.white,
                        ),
                        const SizedBox(width: 8),
                        Text(
                          date,
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Container(
              padding: const EdgeInsets.all(30),
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          const Icon(
                            Icons.person,
                            color: Color(0xFFFF3147),
                          ), // Votre icône
                          const SizedBox(
                              width: 8), // Espace entre l'icône et le texte
                          Text(
                            admin,
                            style: const TextStyle(
                              color: Color(0xFF555555),
                              fontSize: 16,
                            ),
                          ), // Votre texte
                        ],
                      ),
                      Row(
                        children: [
                          const Icon(
                            Icons.chat_bubble,
                            color: Color(0xFFFF3147),
                          ),
                          const SizedBox(width: 8),
                          Text(
                            commentaires,
                            style: const TextStyle(
                              color: Color(0xFF555555),
                              fontSize: 16,
                            ),
                          ), // Votre texte
                        ],
                      ),
                    ],
                  ),
                  const SizedBox(height: 10),
                  Text(
                    title,
                    style: const TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Color(0xFF021738),
                    ),
                  ),
                  const SizedBox(height: 10),
                  SizedBox(
                    width: 300,
                    child: Text(
                      description,
                      style: const TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.normal,
                        color: Color(0xFF555555),
                      ),
                    ),
                  ),
                  const SizedBox(height: 10),
                  TextButton(
                    onPressed: () {},
                    child: const Text(
                      'En Savoir Plus',
                      style: TextStyle(
                          color: Color(0xFFFF3147),
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
