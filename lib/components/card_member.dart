import 'package:flutter/material.dart';

class CardMember extends StatelessWidget {
  final String imagePath;
  final String name;
  final String role;
  final String description;

  const CardMember({
    Key? key,
    required this.imagePath,
    required this.name,
    required this.role,
    required this.description,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Container(
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 245, 245, 245),
          border: Border.all(
            color: Color.fromARGB(255, 228, 228, 228),
            width: 1,
          ),
        ),
        child: Row(
          children: [
            Image.asset(
              imagePath,
              fit: BoxFit.cover,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 30,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                   const SizedBox(
                    height: 15,
                  ),
                  Text(
                    name,
                    style: const TextStyle(
                      color: Color(0xFF021738),
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Text(
                    role,
                    style: const TextStyle(
                      color: Color(0xFF555555),
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  SizedBox(
                    width: 200,
                    child: Text(
                      description,
                      style: const TextStyle(
                        color: Color(0xFF555555),
                        fontSize: 16,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  const Row(
                    children: [
                      Icon(
                        Icons.facebook,
                        color: Color(0xFF555555),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Icon(
                        Icons.rss_feed,
                        color: Color(0xFF555555),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Icon(
                        Icons.wechat_sharp,
                        color: Color(0xFF555555),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Icon(
                        Icons.phone,
                        color: Color(0xFF555555),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
