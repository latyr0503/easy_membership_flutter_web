import 'package:flutter/material.dart';

class CardService extends StatelessWidget {
  final String title;
  final String subtitle;
  final String description;
  final String? imagePath;

  const CardService({
    Key? key,
    required this.title,
    required this.description,
    this.imagePath,
    required this.subtitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Image.asset(
          imagePath!,
          fit: BoxFit.cover,
        ),
        Center(
          heightFactor: 1,
          child: Padding(
            padding: const EdgeInsets.only(
              top: 150,
              left: 33,
            ),
            child: Container(
              width: 300,
              padding: const EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 40,
              ),
              color: Colors.white,
              child:  Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    title,
                    style:const TextStyle(
                      fontSize: 15,
                      color: Color(0xFFFF3147),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(height: 10),
                  Text(
                    subtitle,
                    style: const TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Color(0xFF021738),
                    ),
                  ),
                  const SizedBox(height: 10),
                  Text(
                    description,
                    style: const TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.normal,
                      color: Color(0xFF555555),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(
            top: 310,
            left: 160,
          ),
          child: Container(
            alignment: AlignmentDirectional.centerEnd,
            decoration: BoxDecoration(
              color: const Color(0xFF021738),
              borderRadius: BorderRadius.circular(50.0),
            ),
            child: IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.arrow_forward,
                color: Colors.white,
                size: 25,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
