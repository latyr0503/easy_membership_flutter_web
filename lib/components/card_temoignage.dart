import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class CardTemoignage extends StatelessWidget {
  final String texte;
  final String name;
  final String source;
  final double note;
  final String image;

  const CardTemoignage({
    super.key,
    required this.texte,
    required this.name,
    required this.source,
    required this.note,
    required this.image,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      decoration: const BoxDecoration(
        color: Colors.white,
      ),
      child: Row(
        children: [
          Transform.translate(
              offset: const Offset(-50, 0),
              child: Row(
                children: [
                  Image.asset(image),
                  Transform.translate(
                    offset: const Offset(-25, 25),
                    child: Container(
                      decoration: BoxDecoration(
                        color: const Color(0xFFFF3147),
                        borderRadius: BorderRadius.circular(50.0),
                        border: Border.all(
                          color: Colors.white,
                          width: 5,
                        ),
                      ),
                      child: IconButton(
                        onPressed: () {},
                        icon: const Icon(
                          Icons.format_quote_sharp,
                          color: Colors.white,
                          size: 25,
                        ),
                      ),
                    ),
                  ),
                ],
              )),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                width: 350,
                child: Text(
                  texte,
                  style: const TextStyle(fontSize: 16),
                ),
              ),
              const SizedBox(height: 10),
              Row(
                children: [
                  Text(
                    name,
                    style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Color(0xFF021738)),
                  ),
                  RatingBar.builder(
                    initialRating: note,
                    minRating: 1,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    itemCount: 5,
                    itemPadding: const EdgeInsets.only(right: 0.5),
                    itemBuilder: (context, _) => Transform.scale(
                      scale: 0.5,
                      child: const Icon(
                        Icons.star,
                        color: Color(0xFFFF3147),
                      ),
                    ),
                    onRatingUpdate: (rating) {
                      print(rating);
                    },
                  ),
                ],
              ),
              Text(source)
            ],
          ),
        ],
      ),
    );
  }
}
