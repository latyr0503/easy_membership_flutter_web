import 'package:flutter/material.dart';

class BouttonRounded extends StatelessWidget {
  final IconData icon;
  final Color iconColor;
  final Color backgroundColor;

  const BouttonRounded(
      {super.key,
      required this.icon,
      required this.iconColor,
      required this.backgroundColor});

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: AlignmentDirectional.centerEnd,
      decoration: BoxDecoration(
        color: backgroundColor,
        borderRadius: BorderRadius.circular(50.0),
      ),
      child: IconButton(
        onPressed: () {},
        icon: Icon(
          icon,
          color: iconColor,
          size: 25,
        ),
      ),
    );
  }
}
