import 'package:flutter/material.dart';

class TextReutilisable extends StatelessWidget {
  final String textDescription;
  const TextReutilisable({
    super.key,
    required this.textDescription,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 500,
      child: Text(
        textDescription,
        style: const TextStyle(
          color: Color(0xFF555555),
          fontSize: 18,
          fontWeight: FontWeight.normal,
        ),
      ),
    );
  }
}
