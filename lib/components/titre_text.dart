import 'package:flutter/material.dart';

class TitreText extends StatelessWidget {
  final String title;
  final String subtitle;
  final TextAlign textAlign;
  final Color colorSubtitle;

  const TitreText({
    super.key,
    required this.title,
    required this.subtitle,
    this.textAlign = TextAlign.start,
    this.colorSubtitle = const Color(0xFF021738),
  });

  @override
  Widget build(BuildContext context) {
    return Text.rich(
      textAlign: textAlign,
      TextSpan(
        children: [
          TextSpan(
            text: title,
            style: const TextStyle(
              fontSize: 18,
              color: Color(0xFFFF3147),
              fontWeight: FontWeight.w900,
            ),
          ),
          const TextSpan(
            text: '''

\n''',
          ),
          TextSpan(
            text: subtitle,
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.w900,
              color: colorSubtitle,
            ),
          ),
        ],
      ),
    );
  }
}
