import 'package:easy_membership/pages/home_page.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Easy MemberShip',
      theme: ThemeData(
          visualDensity: VisualDensity.adaptivePlatformDensity,
          fontFamily: 'SpaceGrotesk'),
      home: const HomePage(),
    );
  }
}



// lien de deploiment firebase : https://chatappflutter-529f6.web.app/
// line de deploiment cpanel : https://easymembership.euleukcommunication.sn/