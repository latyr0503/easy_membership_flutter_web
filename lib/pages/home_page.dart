import 'package:easy_membership/widgets/about_section.dart';
import 'package:easy_membership/widgets/adhesion_section.dart';
import 'package:easy_membership/widgets/banner_section.dart';
import 'package:easy_membership/widgets/blog_section.dart';
import 'package:easy_membership/widgets/footer.dart';
import 'package:easy_membership/widgets/member_section.dart';
import 'package:easy_membership/widgets/navbar.dart';
import 'package:easy_membership/widgets/partenaire_section.dart';
import 'package:easy_membership/widgets/service_section.dart';
import 'package:easy_membership/widgets/sous_banner.dart';
import 'package:easy_membership/widgets/temoignage_section.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const PreferredSize(
        preferredSize: Size.fromHeight(100),
        child: Navbar(),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const BannerSection(),
            SousBanner(),
            const SizedBox(
              height: 100,
            ),
            const AboutSection(),
            const SizedBox(
              height: 100,
            ),
            ServiceSection(),
            const SizedBox(
              height: 100,
            ),
            const MemberSection(),
            const SizedBox(
              height: 100,
            ),
            const AdhesionSection(),
            const TemoignageSection(),
            const SizedBox(
              height: 100,
            ),
            const PartenaireSection(),
            const SizedBox(
              height: 100,
            ),
            BlogSection(),
            const SizedBox(
              height: 100,
            ),
            const Footer(),
          ],
        ),
      ),
    );
  }
}
