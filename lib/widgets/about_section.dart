import 'package:easy_membership/components/button.dart';
import 'package:easy_membership/components/image_text.dart';
import 'package:easy_membership/components/text.dart';
import 'package:easy_membership/components/titre_text.dart';
import 'package:flutter/material.dart';

class AboutSection extends StatelessWidget {
  const AboutSection({Key? key});

  @override
  Widget build(BuildContext context) {
    double halfWidth = MediaQuery.of(context).size.width * 0.5;

    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          width: halfWidth,
          child: Padding(
            padding: const EdgeInsets.only(left: 120.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const TitreText(
                  title: 'Qui sommes nous',
                  subtitle:
                      'Excellents services pour\nles associations et les aidants',
                ),
                const SizedBox(
                  height: 15,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const TextReutilisable(
                      textDescription:
                          'Lorem ipsum dolor sit amet consectetur. Ornare blandit morbi viverra id. Purus interdum nunc etiam diam massa feugiat. Faucibus nibh quisque a volutpat id. Duis cras ornare praesent tincidunt.',
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    const Row(
                      children: [
                        ImageText(
                          text: 'Design de sites Web \net développement',
                          image: 'assets/4.png',
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        ImageText(
                          text: 'Applications Android et \niOS développement',
                          image: 'assets/03.png',
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    const TextReutilisable(
                      textDescription:
                          'Lorem ipsum dolor sit amet consectetur. Ornare blandit morbi viverra id. Purus interdum nunc etiam diam massa feugiat. Faucibus nibh quisque a volutpat id. Duis cras ornare praesent tincidunt.',
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    SizedBox(
                      width: 200,
                      child: ButtonReutilisable(
                        onPressed: () {
                          print('button about');
                        },
                        buttonText: "Lorem Ipsum dolor",
                        icon: Icons.arrow_right,
                        backgroundColor: const Color(0xFFFF3147),
                        textColor: Colors.white,
                        iconColor: Colors.white,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        SizedBox(
          width: halfWidth,
          child: Container(
            padding: EdgeInsets.only(left: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset('assets/about_1.png'),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Image.asset('assets/about_2.png'),
                    const SizedBox(
                      width: 20,
                    ),
                    Container(
                      color: const Color(0xFF021738),
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text(
                            "Entreprise \nProgramme d'éthique",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 22,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Image.asset('assets/about_3.png'),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
