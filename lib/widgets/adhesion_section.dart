import 'package:flutter/material.dart';

class AdhesionSection extends StatefulWidget {
  const AdhesionSection({super.key});

  @override
  State<AdhesionSection> createState() => _AdhesionSectionState();
}

class _AdhesionSectionState extends State<AdhesionSection> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          alignment: Alignment.topLeft,
          height: 500,
          child: Image.asset('assets/adhesion.png'),
        ),
        Container(
          alignment: Alignment.topCenter,
          padding: EdgeInsets.only(top: 50, bottom: 50, left: 300),
          child: Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.all(30),
            width: 500,
            height: 400,
            decoration: const BoxDecoration(
              color: Color(0xFFFFFFFF),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Column(
                        children: [
                          Container(
                            color: const Color(0xFFF2F6F9),
                            child: const TextField(
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 20, vertical: 10),
                                border: InputBorder.none, // Remove the border
                                hintText: 'Votre Nom',
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(width: 20),
                    Expanded(
                      child: Column(
                        children: [
                          Container(
                            color: const Color(0xFFF2F6F9),
                            child: const TextField(
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 20, vertical: 10),
                                border: InputBorder.none, // Remove the border
                                hintText: 'Votre adresse e-mail',
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Row(
                  children: [
                    Expanded(
                      child: Column(
                        children: [
                          Container(
                            color: const Color(0xFFF2F6F9),
                            child: const TextField(
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 20, vertical: 10),
                                border: InputBorder.none, // Remove the border
                                hintText: 'Choississez une Association',
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(width: 20),
                    Expanded(
                      child: Column(
                        children: [
                          Container(
                            color: Color(0xFFF2F6F9),
                            child: const TextField(
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 20, vertical: 10),
                                border: InputBorder.none, // Remove the border
                                hintText: 'Choississez ???',
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    border: const Border(),
                    borderRadius: BorderRadius.circular(2.0),
                  ),
                  child: Container(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    color: Color(0xFFF2F6F9),
                    child: TextField(
                      maxLines: 5,
                      decoration: const InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Entrez votre texte ici',
                      ),
                      onChanged: (text) {
                        // Handle the text input here
                      },
                    ),
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                ElevatedButton.icon(
                  onPressed: () {},
                  label: const Text('ADHERER',
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  icon: const Icon(Icons.person),
                  style: ElevatedButton.styleFrom(
                    elevation: 0,
                    backgroundColor: const Color(0xFFFF3147),
                    padding: EdgeInsets.all(16),
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}
