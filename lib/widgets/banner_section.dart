import 'package:easy_membership/components/button.dart';
import 'package:flutter/material.dart';

class BannerSection extends StatelessWidget {
  const BannerSection({Key? key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 600,
      child: Stack(
        fit: StackFit.expand,
        children: [
          Image.asset(
            'assets/image_banner.png',
            fit: BoxFit.cover,
          ),
          Container(
            color: Colors.black.withOpacity(0.5),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 200),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text.rich(
                  TextSpan(
                    children: [
                      TextSpan(
                        text: "Votre Solution D'association \n",
                        style: TextStyle(
                          fontSize: 20,
                          color: Color(0xFFFF3147),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      TextSpan(
                        text: "Disposer votre site \nvitrine en 2 semaines",
                        style: TextStyle(
                          fontSize: 65,
                          fontWeight: FontWeight.w900,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
                Row(
                  children: [
                    ButtonReutilisable(
                      onPressed: () {},
                      buttonText: 'Commercer',
                      icon: Icons.play_arrow,
                      backgroundColor: const Color.fromARGB(255, 255, 255, 255),
                      textColor: Colors.black,
                      iconColor: Colors.black,
                    ),
                    const SizedBox(width: 30),
                    Container(
                      decoration: BoxDecoration(
                        color: const Color(0xFFFF3147),
                        borderRadius: BorderRadius.circular(50.0),
                      ),
                      child: IconButton(
                        onPressed: () {},
                        icon: const Icon(
                          Icons.play_arrow_outlined,
                          color: Colors.white,
                          size: 25,
                        ),
                      ),
                    ),
                    const SizedBox(width: 30),
                    const Text(
                      'Comment Nous Travaillons',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
