import 'package:easy_membership/components/card_blog.dart';
import 'package:easy_membership/components/titre_text.dart';
import 'package:flutter/material.dart';

class BlogSection extends StatelessWidget {
  BlogSection({super.key});

  final List<Map<String, String>> cardData = [
    {
      'imagePath': 'assets/blog_1.png',
      'title': 'Conseils Pour Réussir En Association',
      'commentaires': '13 commentaires',
      'admin': 'Lamine',
      'date': '22 Novembre 2023',
      'description':
          'Uere are full service engage is compan provide solution for employee needs…',
    },
    {
      'imagePath': 'assets/blog_2.png',
      'title': 'Conseils Pour Réussir En Association',
      'commentaires': '39 commentaires',
      'admin': 'Admin',
      'date': '05 Novembre 2023',
      'description':
          'Uere are full service engage is compan provide solution for employee needs…',
    },
    {
      'imagePath': 'assets/blog_3.png',
      'title': 'Conseils Pour Réussir En Association',
      'commentaires': '8 commentaires',
      'admin': 'A. Latyr ',
      'date': '2 Octobre 2023',
      'description':
          'Uere are full service engage is compan provide solution for employee needs…',
    },
  ];
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const TitreText(
          textAlign: TextAlign.center,
          title: 'Nos Reussites',
          subtitle: "Dernières nouvelles et articles\nDans Notre Blog",
        ),
        const SizedBox(
          height: 50,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: cardData.map((data) {
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: CardBlog(
                imagePath: data['imagePath']!,
                title: data['title']!,
                description: data['description']!,
                commentaires: data['commentaires']!,
                admin: data['admin']!,
                date: data['date']!,
              ),
            );
          }).toList(),
        ),
      ],
    );
  }
}
