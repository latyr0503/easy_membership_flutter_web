import 'package:easy_membership/components/icon_rounded.dart';
import 'package:flutter/material.dart';

class Footer extends StatelessWidget {
  const Footer({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: double.infinity,
          color: const Color(0xFF021738),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 100),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                              text: 'Easy ',
                              style: TextStyle(
                                fontSize: 25,
                                color: Color(0xFFFF3147),
                                fontWeight: FontWeight.bold,
                              )),
                          TextSpan(
                            text: 'Membership',
                            style: TextStyle(
                              fontSize: 25,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Text(
                      'A Propos',
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.normal,
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    SizedBox(
                      width: 300,
                      child: Text(
                        'Lorem ipsum dolor sit amet consectetur adipiscing elit et magnis, tellus euismod consequat primis fames himenaeos at faucibus, risus torquent lectus feugiat aliquam porta velit ridiculus.',
                        style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w300,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Row(
                      children: [
                        BouttonRounded(
                          icon: Icons.facebook,
                          backgroundColor: Color(0xFFFF3147),
                          iconColor: Colors.white,
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        BouttonRounded(
                          icon: Icons.markunread,
                          backgroundColor: Color(0xFFFF3147),
                          iconColor: Colors.white,
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        BouttonRounded(
                          icon: Icons.subscriptions,
                          backgroundColor: Color(0xFFFF3147),
                          iconColor: Colors.white,
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        BouttonRounded(
                          icon: Icons.phone,
                          backgroundColor: Color(0xFFFF3147),
                          iconColor: Colors.white,
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(
                  width: 30,
                ),
                Container(
                  width: 250,
                  height: 200,
                  color: Colors.white,
                ),
                const SizedBox(
                  width: 30,
                ),
                Container(
                  width: 250,
                  height: 200,
                  color: Colors.red,
                ),
                const SizedBox(
                  width: 30,
                ),
                Container(
                  width: 250,
                  height: 200,
                  color: Colors.amber,
                ),
              ],
            ),
          ),
        ),
        Container(
          width: double.infinity,
          height: 50,
          color: Colors.white,
          child: const Padding(
            padding: EdgeInsets.only(top: 15),
            child: Text.rich(
              TextSpan(
                children: [
                  TextSpan(
                    text: '© 2023',
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: Color(0xFF555555),
                    ),
                  ),
                  TextSpan(
                      text: ' Easy Membership ',
                      style: TextStyle(
                        fontSize: 15,
                        color: Color(0xFFFF3147),
                        fontWeight: FontWeight.bold,
                      )),
                  TextSpan(
                    text: '. Tous droits réservés.',
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: Color(0xFF555555),
                    ),
                  ),
                ],
              ),
              textAlign: TextAlign.center,
            ),
          ),
        )
      ],
    );
  }
}
