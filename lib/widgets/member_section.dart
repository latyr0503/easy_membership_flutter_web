import 'package:easy_membership/components/card_member.dart';
import 'package:easy_membership/components/titre_text.dart';
import 'package:flutter/material.dart';

class MemberSection extends StatelessWidget {
  const MemberSection({super.key});

  @override
  Widget build(BuildContext context) {
    return const Column(
      children: [
        TitreText(
          textAlign: TextAlign.center,
          title: 'Les Membres De Notre Equipe',
          subtitle:
              "L'expérience Constal à la rencontre des \nemployé de l'équipe",
        ),
        SizedBox(
          height: 50,
        ),
        Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                CardMember(
                  imagePath: 'assets/member_1.png',
                  name: 'Lamine Cisse',
                  role: 'Founder',
                  description: 'Lorem ipsum dolor sit amet consectetur \nadipiscing elit, erat velit platea magnis \ntempor semper curabitur conubia',
                ),
                SizedBox(
                  width: 30,
                ),
                CardMember(
                  imagePath: 'assets/member_2.png',
                  name: 'Rama Fall',
                  role: 'Founder',
                  description: 'Lorem ipsum dolor sit amet consectetur \nadipiscing elit, erat velit platea magnis \ntempor semper curabitur conubia',
                ),
              ],
            ),
            SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                CardMember(
                  imagePath: 'assets/member_3.png',
                  name: 'Nini Senghor',
                  role: 'Founder',
                  description: 'Lorem ipsum dolor sit amet consectetur \nadipiscing elit, erat velit platea magnis \ntempor semper curabitur conubia',
                ),
                SizedBox(
                  width: 30,
                ),
                CardMember(
                  imagePath: 'assets/member_4.png',
                  name: 'Mouhamed Loum',
                  role: 'Founder',
                  description: 'Lorem ipsum dolor sit amet consectetur \nadipiscing elit, erat velit platea magnis \ntempor semper curabitur conubia',
                ),
              ],
            ),
          ],
        ),
      ],
    );
  }
}
