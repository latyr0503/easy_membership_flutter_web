import 'package:easy_membership/components/button.dart';
import 'package:flutter/material.dart';

class Navbar extends StatelessWidget {
  const Navbar({super.key});

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;

    return PreferredSize(
      preferredSize: Size(screenSize.width, 1000),
      child: Container(
        color: Colors.black,
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Row(
            children: [
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                              text: 'Easy ',
                              style: TextStyle(
                                fontSize: 25,
                                color: Color(0xFFFF3147),
                                fontWeight: FontWeight.bold,
                              )),
                          TextSpan(
                            text: 'Membership',
                            style: TextStyle(
                              fontSize: 25,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(width: screenSize.width / 20),
                    InkWell(
                      onTap: () {},
                      child: const Text(
                        'Accueil',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    SizedBox(width: screenSize.width / 30),
                    InkWell(
                      onTap: () {},
                      child: const Row(
                        children: [
                          Text(
                            'Association',
                            style: TextStyle(color: Colors.white),
                          ),
                          Icon(
                            Icons.keyboard_arrow_down,
                            color: Colors.white,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(width: screenSize.width / 30),
                    InkWell(
                      onTap: () {},
                      child: const Row(
                        children: [
                          Text(
                            'Vous Etes Aidant',
                            style: TextStyle(color: Colors.white),
                          ),
                          Icon(
                            Icons.keyboard_arrow_down,
                            color: Colors.white,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(width: screenSize.width / 30),
                    InkWell(
                      onTap: () {},
                      child: const Text(
                        'Contact',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    SizedBox(width: screenSize.width / 30),
                    ButtonReutilisable(
                      onPressed: () {},
                      buttonText: 'ADHERER',
                      icon: Icons.person_3_rounded,
                      backgroundColor: const Color(0xFFFF3147),
                      textColor: Colors.white, iconColor: Colors.white,
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
