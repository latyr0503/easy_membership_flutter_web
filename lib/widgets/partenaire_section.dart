import 'package:flutter/material.dart';

class PartenaireSection extends StatelessWidget {
  const PartenaireSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: [
          _buildImage('assets/partenaire_1.png'),
          _buildImage('assets/partenaire_2.png'),
          _buildImage('assets/partenaire_3.png'),
          _buildImage('assets/partenaire_4.png'),
          _buildImage('assets/partenaire_5.png'),
        ],
      ),
    );
  }

  Widget _buildImage(String imagePath) {
    return Padding(
      padding: const EdgeInsets.only(right: 30.0),
      child: Image.asset(
        imagePath,
        fit: BoxFit.cover,
      ),
    );
  }
}
