import 'package:easy_membership/components/button.dart';
import 'package:easy_membership/components/card_service.dart';
// import 'package:easy_membership/components/card_service.dart';
import 'package:easy_membership/components/titre_text.dart';
import 'package:flutter/material.dart';

class ServiceSection extends StatelessWidget {
  ServiceSection({super.key});

  final List<Map<String, String>> cardData = [
    {
      'imagePath': 'assets/service_1.png',
      'title': 'Design',
      'subtitle': 'Conception App',
      'description':
          'Lorem ipsum dolor sit amet consectetur. Luctus nibh dolor cras',
    },
    {
      'imagePath': 'assets/service_2.png',
      'title': 'Marketing',
      'subtitle': 'Recherche En Marketing',
      'description':
          'Lorem ipsum dolor sit amet consectetur. Luctus nibh dolor cras',
    },
    {
      'imagePath': 'assets/service_3.png',
      'title': 'Développement',
      'subtitle': 'Développement web',
      'description':
          'Lorem ipsum dolor sit amet consectetur. Luctus nibh dolor cras',
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: 800,
      color: const Color(0xFFF2F6F9),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const SizedBox(
            height: 100,
          ),
          const TitreText(
            textAlign: TextAlign.center,
            title: 'Nos Services',
            subtitle:
                "Nous sommes la meilleure offre de services \npour les associations et les aidants",
          ),
          const SizedBox(
            height: 50,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: cardData.map((data) {
              return Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: CardService(
                  imagePath: data['imagePath']!,
                  title: data['title']!,
                  description: data['description']!,
                  subtitle: data['subtitle']!,
                ),
              );
            }).toList(),
          ),
          const SizedBox(height: 50),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                width: 200,
                child: ButtonReutilisable(
                  onPressed: () {
                    print('button service');
                  },
                  buttonText: "Lorem Ipsum dolor",
                  icon: Icons.arrow_right,
                  backgroundColor: const Color(0xFFFF3147),
                  textColor: Colors.white,
                  iconColor: Colors.white,
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 100,
          ),
        ],
      ),
    );
  }
}
