import 'package:easy_membership/components/card.dart';
import 'package:flutter/material.dart';

class SousBanner extends StatelessWidget {
  // ignore: use_key_in_widget_constructors
  SousBanner({Key? key});

  final List<Map<String, String>> cardData = [
    {
      'imagePath': 'assets/01.png',
      'title': 'Lorem ipsum dolor',
      'description':
          'Lorem ipsum dolor sit amet consectetur adipiscing elit  .',
    },
    {
      'imagePath': 'assets/02.png',
      'title': 'Lorem ipsum dolor',
      'description':
          'Lorem ipsum dolor sit amet consectetur adipiscing elit  .',
    },
    {
      'imagePath': 'assets/03.png',
      'title': 'Lorem ipsum dolor',
      'description':
          'Lorem ipsum dolor sit amet consectetur adipiscing elit  .',
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color(0xFF021738),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const SizedBox(
            height: 100,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: cardData.map((data) {
              return Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: CardSection(
                  imagePath: data['imagePath']!,
                  title: data['title']!,
                  description: data['description']!,
                ),
              );
            }).toList(),
          ),
          const SizedBox(height: 50),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                'Les défis ne sont que des opportunités déguisées.',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.normal,
                ),
              ),
              TextButton(
                onPressed: () {
                  // ignore: avoid_print
                  print('hello world');
                },
                child: const Text(
                  'Acceptes le challenge!',
                  style: TextStyle(
                    color: Color(0xFFFF3147),
                    fontSize: 18,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 50,
          ),
        ],
      ),
    );
  }
}
