import 'package:easy_membership/components/card_temoignage.dart';
import 'package:easy_membership/components/titre_text.dart';
import 'package:flutter/material.dart';

class TemoignageSection extends StatelessWidget {
  const TemoignageSection({super.key});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: 400,
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/Temoignage_section.png'),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 100.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const TitreText(
                    colorSubtitle: Colors.white,
                    title: 'Témoignages',
                    subtitle:
                        'Ce que disent les clients\nÀ propos de nos services',
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          color: const Color.fromRGBO(255, 255, 255, 0.3),
                          borderRadius: BorderRadius.circular(50.0),
                        ),
                        child: IconButton(
                          onPressed: () {},
                          icon: const Icon(
                            Icons.arrow_back,
                            color: Colors.white,
                            size: 25,
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: const Color.fromRGBO(255, 255, 255, 0.3),
                          borderRadius: BorderRadius.circular(50.0),
                        ),
                        child: IconButton(
                          onPressed: () {},
                          icon: const Icon(
                            Icons.arrow_forward,
                            color: Colors.white,
                            size: 25,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              const CardTemoignage(
                texte: 'Lorem ipsum dolor sit amet consectetur  elit facilisis lacus torquent,  et  pretium luctus auctor nisl pulvinar ante, rutrum lectus elementum sodales idunt.',
                name: 'Maimouna Ndiye ',
                image: 'assets/temoignage_person_1.png',
                note: 4,
                source: 'prospet',
              )
            ],
          ),
        ),
      ],
    );
  }
}
